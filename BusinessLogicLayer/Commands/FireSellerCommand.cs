﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics.Contracts;

using DomainModel.Entities;
using DataAccessLayer.Gateway;

namespace BusinessLogicLayer.Commands
{
    public class FireSellerCommand : Command, ICommand
    {
        private readonly ISellerGateway _sellerGateway;
        private readonly Seller _seller;

        public FireSellerCommand(ISellerGateway gateway, Seller seller)
        {
            Contract.Requires(gateway != null);
            Contract.Requires(seller != null);

            _sellerGateway = gateway;
            _seller = seller;
        }

        public override void Execute()
        {
            _sellerGateway.FireSeller(_seller);
        }
    }
}
