﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StructureMap;

using System.Windows.Forms;

namespace ApplicationServicesLayer 
{
    public class ApplicationController : IApplicationController
    {
        private static IContainer _container;

        public ApplicationController(System.Windows.Forms.ApplicationContext context)
        {
            _container = new Container(x => {
                x.For<IApplicationController>().Use<ApplicationController>(this);
                x.For<ApplicationContext>().Use(context);
            });
        }

        public static IApplicationController Instance()
        {
            return _container.GetInstance<IApplicationController>();
        }

        public void Configure(Action<ConfigurationExpression> configure)
        {
            _container.Configure(configure);
        }


        public T GetInstance<T>()
        {
            return _container.GetInstance<T>();
        }
    }
}
