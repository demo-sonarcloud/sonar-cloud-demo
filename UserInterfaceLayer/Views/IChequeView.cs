﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UserInterfaceLayer.Events;

namespace UserInterfaceLayer.Views
{
    public interface IChequeView : IView
    {
        event Action Closing;
        event Action ChequeCancelPressed;
        event EventHandler<ChequeCancellingEventArgs> ChequeCancelling;

        void ShowCancellingDialog();

        void EnableCancelling(bool enabled);

        void ClearChequeItems();

        void AddChequeItem(string custom, string warehouse, string price, string count, string sum);
    }
}
