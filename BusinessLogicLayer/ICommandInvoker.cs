﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;

namespace BusinessLogicLayer.Commands
{
    public interface ICommandInvoker
    {
        void AddSeller(Seller seller);
        void FireSeller(Seller seller);

        SellerBonus CalculateSellerBonus(Seller seller);

        IEnumerable<Seller> GetAllSellers();
        IEnumerable<Seller> GetWorkingSellers();

        IEnumerable<Warehouse> GetAllWarehouses();
        IEnumerable<Warehouse> GetRetailWarehouses();

        IEnumerable<Custom> GetAllCustoms();

        IEnumerable<CountedCustom> GetAllCustoms(Warehouse warehouse);

        IEnumerable<Cheque> GetAllCheques();
        IEnumerable<ChequeItem> GetChequeItems(Cheque cheque);

        void CreateCheque(SellerId sellerId, IEnumerable<ChequeItem> items);
        void CancelCheque(Cheque cheque);
    }
}
