﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Entities
{
    public class Cheque
    {
        public ChequeId ID { get; set; }

        public DateTime Date { get; set; }

        public SellerId SellerID { get; set; }

        public ChequeCancellationState CancellationState { get; set; }

        public Cheque(ChequeId id,
            DateTime date,
            SellerId sellerID,
            ChequeCancellationState cancellationState)
        {
            ID = id;
            Date = date;
            SellerID = sellerID;
            CancellationState = cancellationState;
        }
    }
}
