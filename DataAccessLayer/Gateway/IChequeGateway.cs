﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;

using System.Data;

namespace DataAccessLayer.Gateway
{
    public interface IChequeGateway
    {
        IEnumerable<Cheque> GetAllCheques();
        IEnumerable<ChequeItem> GetChequeItems(Cheque cheque);

        ChequeId CreateCheque(SellerId id);
        void CancelCheque(Cheque cheque);
        void AddChequeItems(ChequeId id, IEnumerable<ChequeItem> items);
    }
}
