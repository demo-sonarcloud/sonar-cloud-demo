﻿namespace UserInterfaceLayer.Forms
{
    partial class SelectCustomNomenclatureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._customsListBox = new System.Windows.Forms.ListBox();
            this._searchTextBox = new System.Windows.Forms.TextBox();
            this._addCustomButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _customsListBox
            // 
            this._customsListBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._customsListBox.FormattingEnabled = true;
            this._customsListBox.Location = new System.Drawing.Point(0, 37);
            this._customsListBox.Name = "_customsListBox";
            this._customsListBox.Size = new System.Drawing.Size(284, 225);
            this._customsListBox.TabIndex = 0;
            // 
            // _searchTextBox
            // 
            this._searchTextBox.Location = new System.Drawing.Point(12, 11);
            this._searchTextBox.Name = "_searchTextBox";
            this._searchTextBox.Size = new System.Drawing.Size(148, 20);
            this._searchTextBox.TabIndex = 1;
            this._searchTextBox.Visible = false;
            // 
            // _addCustomButton
            // 
            this._addCustomButton.Location = new System.Drawing.Point(166, 9);
            this._addCustomButton.Name = "_addCustomButton";
            this._addCustomButton.Size = new System.Drawing.Size(106, 23);
            this._addCustomButton.TabIndex = 2;
            this._addCustomButton.Text = "Добавить";
            this._addCustomButton.UseVisualStyleBackColor = true;
            this._addCustomButton.Click += new System.EventHandler(this._addCustomButton_Click);
            // 
            // SelectCustomNomenclatureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this._addCustomButton);
            this.Controls.Add(this._searchTextBox);
            this.Controls.Add(this._customsListBox);
            this.Name = "SelectCustomNomenclatureForm";
            this.Text = "Выбор номенклатуры";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SelectCustomNomenclatureForm_FormClosing);
            this.Load += new System.EventHandler(this.SelectCustomForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox _customsListBox;
        private System.Windows.Forms.TextBox _searchTextBox;
        private System.Windows.Forms.Button _addCustomButton;
    }
}