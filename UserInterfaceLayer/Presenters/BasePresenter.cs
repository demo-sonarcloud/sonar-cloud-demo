﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UserInterfaceLayer.Views;

using ApplicationServicesLayer;

namespace UserInterfaceLayer.Presenters
{
    public abstract class BasePresenter<TView> : IPresenter
        where TView : IView
    {
        protected IApplicationController Controller { get; private set; }
        protected TView View { get; private set; }

        public BasePresenter(IApplicationController controller, TView view)
        {
            Controller = controller;
            View = view;
        }

        public virtual void Show()
        {
            View.Show();
        }

        public virtual void Hide()
        {
            View.Hide();
        }

        public virtual void Activate()
        {
            View.Activate();
        }

        public virtual void Close()
        {
            View.Close();
        }
    }
}
