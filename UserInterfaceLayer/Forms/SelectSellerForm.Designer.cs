﻿namespace UserInterfaceLayer.Forms
{
    partial class SelectSellerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._loginButton = new System.Windows.Forms.Button();
            this._splitContainer = new System.Windows.Forms.SplitContainer();
            this._sellersList = new System.Windows.Forms.ListBox();
            this._sellerLabel = new System.Windows.Forms.Label();
            this._warehousesList = new System.Windows.Forms.ListBox();
            this._warehouseLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this._splitContainer)).BeginInit();
            this._splitContainer.Panel1.SuspendLayout();
            this._splitContainer.Panel2.SuspendLayout();
            this._splitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // _loginButton
            // 
            this._loginButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._loginButton.Location = new System.Drawing.Point(0, 217);
            this._loginButton.Name = "_loginButton";
            this._loginButton.Size = new System.Drawing.Size(284, 45);
            this._loginButton.TabIndex = 1;
            this._loginButton.Text = "Войти";
            this._loginButton.UseVisualStyleBackColor = true;
            this._loginButton.Click += new System.EventHandler(this._loginButton_Click);
            // 
            // _splitContainer
            // 
            this._splitContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this._splitContainer.Location = new System.Drawing.Point(0, 0);
            this._splitContainer.Name = "_splitContainer";
            // 
            // _splitContainer.Panel1
            // 
            this._splitContainer.Panel1.Controls.Add(this._sellersList);
            this._splitContainer.Panel1.Controls.Add(this._sellerLabel);
            // 
            // _splitContainer.Panel2
            // 
            this._splitContainer.Panel2.Controls.Add(this._warehousesList);
            this._splitContainer.Panel2.Controls.Add(this._warehouseLabel);
            this._splitContainer.Size = new System.Drawing.Size(284, 211);
            this._splitContainer.SplitterDistance = 148;
            this._splitContainer.TabIndex = 3;
            // 
            // _sellersList
            // 
            this._sellersList.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._sellersList.FormattingEnabled = true;
            this._sellersList.Location = new System.Drawing.Point(0, 38);
            this._sellersList.Name = "_sellersList";
            this._sellersList.Size = new System.Drawing.Size(148, 173);
            this._sellersList.TabIndex = 2;
            // 
            // _sellerLabel
            // 
            this._sellerLabel.Location = new System.Drawing.Point(3, 9);
            this._sellerLabel.Name = "_sellerLabel";
            this._sellerLabel.Size = new System.Drawing.Size(142, 18);
            this._sellerLabel.TabIndex = 1;
            this._sellerLabel.Text = "Продавец";
            // 
            // _warehousesList
            // 
            this._warehousesList.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._warehousesList.FormattingEnabled = true;
            this._warehousesList.Location = new System.Drawing.Point(0, 38);
            this._warehousesList.Name = "_warehousesList";
            this._warehousesList.Size = new System.Drawing.Size(132, 173);
            this._warehousesList.TabIndex = 4;
            // 
            // _warehouseLabel
            // 
            this._warehouseLabel.Location = new System.Drawing.Point(3, 9);
            this._warehouseLabel.Name = "_warehouseLabel";
            this._warehouseLabel.Size = new System.Drawing.Size(100, 18);
            this._warehouseLabel.TabIndex = 3;
            this._warehouseLabel.Text = "Склад";
            // 
            // SelectSellerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this._splitContainer);
            this.Controls.Add(this._loginButton);
            this.Name = "SelectSellerForm";
            this.Text = "Выбор продавца";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SelectSellerForm_FormClosing);
            this.Load += new System.EventHandler(this.SelectSellerForm_Load);
            this._splitContainer.Panel1.ResumeLayout(false);
            this._splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._splitContainer)).EndInit();
            this._splitContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _loginButton;
        private System.Windows.Forms.SplitContainer _splitContainer;
        private System.Windows.Forms.Label _sellerLabel;
        private System.Windows.Forms.Label _warehouseLabel;
        private System.Windows.Forms.ListBox _sellersList;
        private System.Windows.Forms.ListBox _warehousesList;
    }
}