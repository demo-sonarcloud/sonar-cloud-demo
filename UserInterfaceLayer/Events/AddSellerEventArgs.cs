﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics.Contracts;

namespace UserInterfaceLayer.Events
{
    public class SellerAddedEventArgs : EventArgs
    {
        public string SellerName { get; private set; }
        public string SellerTown { get; private set; }
        public string SellerPhone { get; private set; }

        public SellerAddedEventArgs(string sellerName, string sellerTown, string sellerPhone)
        {
            Contract.Requires(sellerName != null);

            SellerName = sellerName;
            SellerTown = sellerTown;
            SellerPhone = sellerPhone;
        }
    }
}
