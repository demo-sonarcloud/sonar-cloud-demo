﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;

using UserInterfaceLayer.Events;

namespace UserInterfaceLayer.Views
{
    public interface ISelectCustomNomenclatureView : IView
    {
        event Action Closing;

        event Action Loaded;
        event EventHandler<CustomNomenclatureSelectedEventArgs> CustomNomenclatureSelected;

        void LoadCustoms(IEnumerable<string> customs);
    }
}
