﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;

namespace UserInterfaceLayer.Presenters
{
    public interface ISellPresenter : IPresenter
    {
        //Seller CurrentSeller { get; set; }
        //Warehouse CurrentWarehouse { get; set; }

        void Initialize(Seller seller, Warehouse warehouse);
    }
}
