﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using UserInterfaceLayer.Views;
using UserInterfaceLayer.Events;

using DomainModel.Entities;

namespace UserInterfaceLayer.Forms
{
    public partial class SellForm : Form, ISellView
    {
        private readonly ApplicationContext _context;

        public new event Action Closing;
        public event Action Exit;

        public event Action CustomAdding;
        public event Action ChequeCreating;

        public event EventHandler<ChequeItemCountValidatingEventArgs> ChequeItemCountValidating;
        public event EventHandler<ChequeItemCountChangedEventArgs> ChequeItemCountChanged;
        public event EventHandler<ChequeItemRemovedEventArgs> ChequeItemRemoved;

        private bool _cellValidated;

        public SellForm(ApplicationContext context)
        {
            _context = context;
            InitializeComponent();
        }

        public new void Show()
        {
            _context.MainForm = this;
            base.Show();
        }

        public void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }

        public void ShowMessage(string message, string caption)
        {
            MessageBox.Show(message, caption);
        }

        public void ShowError(string errorMessage)
        {
            MessageBox.Show(errorMessage);
        }

        public void ShowError(string errorMessage, string errorCaption)
        {
            MessageBox.Show(errorMessage, errorCaption);
        }

        public void ShowExitDialog()
        {
            if (MessageBox.Show("Sure?", "Exit", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                Exit();
            }
        }

        public void ClearChequeItems()
        {
            _dataGridView.Rows.Clear();
        }

        public void AddChequeItem(Custom custom, Warehouse warehouse, CustomCount count)
        {
            _dataGridView.Rows.Add(custom.Nomenclature.Nomeclature, warehouse.Name.Name, custom.Price.Price, count.Count, custom.Price.Price * count.Count);
        }

        public void ValidateCell(bool validated)
        {
            _cellValidated = validated;
        }

        public void UpdateCountColumn(int rowIndex, decimal count)
        {
            _dataGridView.Rows[rowIndex].Cells["CountColumn"].Value = count;
        }

        public void UpdateSumColumn(int rowIndex, decimal sum)
        {
            _dataGridView.Rows[rowIndex].Cells["SumColumn"].Value = sum;
        }

        private void _addButton_Click(object sender, EventArgs e)
        {
            CustomAdding();
        }

        private void createChequeButton_Click(object sender, EventArgs e)
        {
            ChequeCreating();
        }

        private void _dataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                string countText = _dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value as string;
                ChequeItemCountChanged(this, new ChequeItemCountChangedEventArgs(e.RowIndex, countText));
            }
        }

        private void _dataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (_dataGridView.Columns[e.ColumnIndex].Name == "CountColumn")
            {
                ChequeItemCountValidating(this, new ChequeItemCountValidatingEventArgs(e.FormattedValue));
                if (!_cellValidated)
                {
                    e.Cancel = true;
                }
            }
        }

        private void _dataGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (_dataGridView.Columns[e.ColumnIndex].Name == "CountColumn")
            {
                Console.WriteLine("Validated");
            }
        }

        private void _dataGridView_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            ChequeItemRemoved(this, new ChequeItemRemovedEventArgs(e.RowCount, e.RowIndex));
        }

        private void SellForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Closing();
            e.Cancel = true;
        }
    }
}
