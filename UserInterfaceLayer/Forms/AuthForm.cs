﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using UserInterfaceLayer.Views;
using UserInterfaceLayer.Events;

namespace UserInterfaceLayer.Forms
{
    public partial class AuthForm : Form, IAuthView
    {
        public event EventHandler<LoggingInEventArgs> LoggingIn;

        private readonly ApplicationContext _context;

        public AuthForm(ApplicationContext context)
        {
            _context = context;
            InitializeComponent();
        }

        public new void Show()
        {
            _context.MainForm = this;
            base.Show();
        }

        public new void Close()
        {
            base.Close();
        }

        public void ShowError(string errorMessage)
        {
            MessageBox.Show(errorMessage);
        }

        public void ShowError(string errorMessage, string errorCaption)
        {
            MessageBox.Show(errorMessage, errorCaption);
        }


        private void _sellerButton_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (button.Equals(_sellerButton))
            {
                LoggingIn(this, new LoggingInEventArgs(AuthType.SELLER));
            }
            else if (button.Equals(_managerButton))
            {
                LoggingIn(this, new LoggingInEventArgs(AuthType.MANAGER));
            }
        }
    }
}
