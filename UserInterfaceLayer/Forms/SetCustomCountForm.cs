﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using UserInterfaceLayer.Views;
using UserInterfaceLayer.Events;

namespace UserInterfaceLayer.Forms
{
    public partial class SetCustomCountForm : Form, ISetCustomCountView
    {
        private readonly ApplicationContext _context;

        public new event Action Closing;
        public event EventHandler<CustomCountSettingEventArgs> CustomCountSetting;

        public SetCustomCountForm(ApplicationContext context)
        {
            _context = context;
            InitializeComponent();
        }

        public new void Show()
        {
            _context.MainForm = this;
            base.Show();
        }

        public new void Hide()
        {
            base.Hide();
        }

        public new void Close()
        {
            base.Close();
        }

        public void ShowError(string errorMessage)
        {
            MessageBox.Show(errorMessage);
        }

        public void ShowError(string errorMessage, string errorCaption)
        {
            MessageBox.Show(errorMessage, errorCaption);
        }


        public void ClearCount()
        {
            //_countTextBox.Text = string.Empty;
            _countTextBox.Value = _countTextBox.Minimum;
        }

        public void HighlightCount()
        {
            _countTextBox.Focus();
            _countTextBox.Select(0, _countTextBox.Text.Length);
        }

        private void _setCountButton_Click(object sender, EventArgs e)
        {
            /*string countText = _countTextBox.Text;
            if (countText != string.Empty)
            {
                CustomCountSetting(this, new CustomCountSettingEventArgs(countText));
            }*/
            decimal count = _countTextBox.Value;
            CustomCountSetting(this, new CustomCountSettingEventArgs(count));
        }

        private void SetCustomCountForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Closing();
            e.Cancel = true;
        }
    }
}
