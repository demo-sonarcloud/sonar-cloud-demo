﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataAccessLayer.Gateway;
using DomainModel.Entities;

namespace BusinessLogicLayer.Commands
{
    public class GetAllChequesCommand : Command<IEnumerable<Cheque>>, ICommand<IEnumerable<Cheque>>
    {
        private readonly IChequeGateway _chequeGateway;

        public GetAllChequesCommand(IChequeGateway gateway)
        {
            _chequeGateway = gateway;
        }

        public override IEnumerable<Cheque> Execute()
        {
            return _chequeGateway.GetAllCheques();
        }
    }
}
