﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;
using DataAccessLayer.Gateway;

namespace BusinessLogicLayer.Commands
{
    public class GetAllSellersCommand : Command<IEnumerable<Seller>>, ICommand<IEnumerable<Seller>>
    {
        private readonly ISellerGateway _sellerGateway;

        public GetAllSellersCommand(ISellerGateway gateway)
        {
            _sellerGateway = gateway;
        }

        public override IEnumerable<Seller> Execute()
        {
            return _sellerGateway.GetAllSellers();
        }
    }
}
