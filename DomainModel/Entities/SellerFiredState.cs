﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Entities
{
    public class SellerFiredState
    {
        public bool IsFired { get; set; }

        public SellerFiredState(bool isFired)
        {
            IsFired = isFired;
        }
    }
}
