﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Events
{
    public class LoginEventArgs : EventArgs
    {
        public int SellerIndex { get; private set; }
        public int WarehouseIndex { get; private set; }

        public LoginEventArgs(int sellerIndex, int warehouseIndex)
        {
            SellerIndex = sellerIndex;
            WarehouseIndex = warehouseIndex;
        }
    }
}
