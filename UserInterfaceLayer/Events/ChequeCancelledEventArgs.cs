﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;

namespace UserInterfaceLayer.Events
{
    public class ChequeCancelledEventArgs : EventArgs
    {
        public Cheque Cheque { get; private set; }

        public ChequeCancelledEventArgs(Cheque cheque)
        {
            Cheque = cheque;
        }
    }
}
