﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;
using DataAccessLayer.Gateway;

namespace BusinessLogicLayer.Commands
{
    public class GetWorkingSellersCommand : Command<IEnumerable<Seller>>
    {
        private readonly ISellerGateway _sellerGateway;

        public GetWorkingSellersCommand(ISellerGateway gateway)
        {
            _sellerGateway = gateway;
        }

        public override IEnumerable<Seller> Execute()
        {
            return _sellerGateway.GetWorkingSellers();
        }
    }
}
