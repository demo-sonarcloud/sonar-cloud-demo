﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;

using DomainModel.Entities;

namespace DataAccessLayer.Gateway
{
    public interface ISellerGateway
    {
        
        void AddSeller(Seller seller);
        void FireSeller(Seller seller);

        SellerBonus CalculateSellerBonus(Seller seller);

        IEnumerable<Seller> GetAllSellers();
        IEnumerable<Seller> GetWorkingSellers();
    }
}
