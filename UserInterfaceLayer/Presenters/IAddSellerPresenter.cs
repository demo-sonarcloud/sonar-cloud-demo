﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UserInterfaceLayer.Events;

namespace UserInterfaceLayer.Presenters
{
    public interface IAddSellerPresenter : IPresenter
    {
        event EventHandler<SellerAddedEventArgs> SellerAdded;

        void Initialize();
    }
}
