﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Entities
{
    public class OperationId : EntityId
    {
        public OperationId(int id) : base(id)
        {
        }
    }
}
