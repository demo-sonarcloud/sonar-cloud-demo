﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;
using DataAccessLayer.Gateway;

namespace BusinessLogicLayer.Commands
{
    public class GetWarehouseCustomsCountCommand : Command<IEnumerable<CountedCustom>>
    {
        private readonly ICustomWarehouseGateway _customWarehouseGateway;

        private readonly Warehouse _warehouse;

        public GetWarehouseCustomsCountCommand(ICustomWarehouseGateway gateway, Warehouse warehouse)
        {
            _customWarehouseGateway = gateway;
            _warehouse = warehouse;
        }

        public override IEnumerable<CountedCustom> Execute()
        {
            return _customWarehouseGateway.GetAllCustoms(_warehouse);
        }
    }
}
