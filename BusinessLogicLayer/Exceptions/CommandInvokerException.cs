﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class CommandInvokerException : Exception
    {
        public CommandInvokerException(string message)
            : base(message)
        {
        }

        public CommandInvokerException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
