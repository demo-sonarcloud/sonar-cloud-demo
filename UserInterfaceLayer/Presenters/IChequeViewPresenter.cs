﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UserInterfaceLayer.Events;
using DomainModel.Entities;

namespace UserInterfaceLayer.Presenters
{
    public interface IChequeViewPresenter : IPresenter
    {
        event EventHandler<ChequeCancellingEventArgs> ChequeCancelling;

        void Initialize(Cheque cheque);
    }
}
