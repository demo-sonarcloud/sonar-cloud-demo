﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataAccessLayer.Gateway;
using DomainModel.Entities;

namespace BusinessLogicLayer.Commands
{
    public class CalculateSellerBonusCommand : Command<SellerBonus>, ICommand<SellerBonus>
    {
        private readonly ISellerGateway _sellerGateway;

        private readonly Seller _seller;

        public CalculateSellerBonusCommand(ISellerGateway gateway, Seller seller)
        {
            _sellerGateway = gateway;
            _seller = seller;
        }

        public override SellerBonus Execute()
        {
            return _sellerGateway.CalculateSellerBonus(_seller);
        }
    }
}
