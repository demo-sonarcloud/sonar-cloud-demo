﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using UserInterfaceLayer.Views;
using UserInterfaceLayer.Events;

using DomainModel.Entities;

namespace UserInterfaceLayer.Forms
{
    public partial class SelectSellerForm : Form, ISelectSellerView
    {
        private readonly ApplicationContext _context;

        public event Action Loaded;
        public new event Action Closing;
        public event EventHandler<LoginEventArgs> Login;

        public SelectSellerForm(ApplicationContext context)
        {
            _context = context;
            InitializeComponent();       
        }

        public new void Show()
        {
            _context.MainForm = this;
            base.Show();
        }

        public void ShowError(string errorMessage)
        {
            MessageBox.Show(errorMessage);
        }
        
        public void ShowError(string errorMessage, string errorCaption)
        {
            MessageBox.Show(errorMessage, errorCaption);
        }

        public void LoadSellers(IEnumerable<string> sellers)
        {
            _sellersList.Items.Clear();
            _sellersList.Items.AddRange(sellers.ToArray());
        }

        public void LoadWarehouses(IEnumerable<string> warehouses)
        {
            _warehousesList.Items.Clear();
            _warehousesList.Items.AddRange(warehouses.ToArray());
        }

        private void SelectSellerForm_Load(object sender, EventArgs e)
        {
            Loaded();
        }

        private void _loginButton_Click(object sender, EventArgs e)
        {
            int sellerIndex = _sellersList.SelectedIndex;
            int warehouseIndex = _warehousesList.SelectedIndex;
            Login(this, new LoginEventArgs(sellerIndex, warehouseIndex));
        }

        private void SelectSellerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Closing();
            e.Cancel = true;
        }
    }
}
