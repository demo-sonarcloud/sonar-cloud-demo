﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class ChequeGatewayException : Exception
    {
        public ChequeGatewayException(string message)
            : base(message)
        {
        }

        public ChequeGatewayException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
