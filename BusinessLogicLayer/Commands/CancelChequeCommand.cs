﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics.Contracts;

using DataAccessLayer.Gateway;
using DomainModel.Entities;

namespace BusinessLogicLayer.Commands
{
    public class CancelChequeCommand : Command, ICommand
    {
        private readonly IChequeGateway _chequeGateway;
        private readonly Cheque _cheque;

        public CancelChequeCommand(IChequeGateway gateway, Cheque cheque)
        {
            Contract.Requires(gateway != null);
            Contract.Requires(cheque != null);

            _chequeGateway = gateway;
            _cheque = cheque;
        }

        public override void Execute()
        {
            _chequeGateway.CancelCheque(_cheque);
        }
    }
}
