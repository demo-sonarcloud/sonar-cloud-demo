﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using UserInterfaceLayer.Views;
using UserInterfaceLayer.Events;

using DomainModel.Entities;


namespace UserInterfaceLayer.Forms
{
    public partial class SelectCustomNomenclatureForm : Form, ISelectCustomNomenclatureView
    {
        private readonly ApplicationContext _context;

        public new event Action Closing;

        public event Action Loaded;
        public event EventHandler<CustomNomenclatureSelectedEventArgs> CustomNomenclatureSelected;

        public SelectCustomNomenclatureForm(ApplicationContext context)
        {
            _context = context;
            InitializeComponent();
        }

        public new void Show()
        {
            _context.MainForm = this;
            base.Show();
        }

        public void ShowError(string errorMessage)
        {
            MessageBox.Show(errorMessage);
        }

        public void ShowError(string errorMessage, string errorCaption)
        {
            MessageBox.Show(errorMessage, errorCaption);
        }

        public void LoadCustoms(IEnumerable<string> customs)
        {
            _customsListBox.Items.Clear();
            _customsListBox.Items.AddRange(customs.ToArray());
            /*foreach (Custom custom in customs)
            {
                _customsListBox.Items.Add(custom.Nomenclature);
            }*/
        }

        private void SelectCustomForm_Load(object sender, EventArgs e)
        {
            Loaded();
        }

        private void _addCustomButton_Click(object sender, EventArgs e)
        {
            int selectedIndex = _customsListBox.SelectedIndex;
            CustomNomenclatureSelected(this, new CustomNomenclatureSelectedEventArgs(selectedIndex));
        }

        private void SelectCustomNomenclatureForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Closing();
            e.Cancel = true;
        }
    }
}
