﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Events
{
    public class SellerBonusCalculatingEventArgs : EventArgs
    {
        public int SellerIndex { get; private set; }

        public SellerBonusCalculatingEventArgs(int sellerIndex)
        {
            SellerIndex = sellerIndex;
        }
    }
}
