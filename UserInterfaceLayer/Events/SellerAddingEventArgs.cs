﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Events
{
    public class SellerAddingEventArgs : EventArgs
    {
        public int SellerIndex { get; private set; }

        public SellerAddingEventArgs(int sellerIndex)
        {
            SellerIndex = sellerIndex;
        }
    }
}
