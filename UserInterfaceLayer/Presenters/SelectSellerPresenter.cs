﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UserInterfaceLayer.Views;

using DataAccessLayer.Gateway;
using DomainModel.Entities;

using ApplicationServicesLayer;
using BusinessLogicLayer.Commands;

using UserInterfaceLayer.Events;

namespace UserInterfaceLayer.Presenters
{
    public class SelectSellerPresenter : BasePresenter<ISelectSellerView>, ISelectSellerPresenter 
    {
        private Seller[] _sellers;
        private Warehouse[] _warehouses;

        public SelectSellerPresenter(IApplicationController controller, ISelectSellerView view) : base(controller, view)
        {
            View.Loaded += OnLoaded;
            View.Closing += OnClosing;
            View.Login += new EventHandler<LoginEventArgs>(OnLogin);
        }

        public void Initialize()
        {
            try
            {
                _sellers = Controller.GetInstance<ICommandInvoker>().GetWorkingSellers().ToArray();
                _warehouses = Controller.GetInstance<ICommandInvoker>().GetRetailWarehouses().ToArray();
                View.LoadSellers(_sellers.Select(s => s.Name.FullName));
                View.LoadWarehouses(_warehouses.Select(w => w.Name.Name));
            }
            catch (Exception e)
            {
                View.ShowError(e.Message, "Error");
            }
        }

        private void OnLoaded()
        {
        }

        private void OnClosing()
        {
            IAuthPresenter presenter = Controller.GetInstance<IAuthPresenter>();
            presenter.Show();
            presenter.Activate();
            View.Hide();
        }

        private void OnLogin(object sender, LoginEventArgs indices)
        {
            if (indices.SellerIndex >= 0 && indices.WarehouseIndex >= 0)
            {
                Seller seller = _sellers[indices.SellerIndex];
                Warehouse warehouse = _warehouses[indices.WarehouseIndex];
                Console.WriteLine("Logged in as seller {0}-{1}", seller.ID, seller.Name);

                ISellPresenter presenter = Controller.GetInstance<ISellPresenter>();

                presenter.Initialize(seller, warehouse);
                presenter.Show();
                presenter.Activate();

                View.Hide();
            }
            else
            {
                View.ShowError("Seller and warehouse not chosen", "Please select seller and warehouse");
            }
            
        }
    }
}
