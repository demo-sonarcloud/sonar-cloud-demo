﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Events
{
    public class ChequeSelectedEventArgs : EventArgs
    {
        public int ChequeIndex { get; private set; }

        public ChequeSelectedEventArgs(int chequeIndex)
        {
            ChequeIndex = chequeIndex;
        }
    }
}
