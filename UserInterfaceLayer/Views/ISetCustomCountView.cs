﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UserInterfaceLayer.Events;

namespace UserInterfaceLayer.Views
{
    public interface ISetCustomCountView : IView
    {
        event Action Closing;
        event EventHandler<CustomCountSettingEventArgs> CustomCountSetting;

        void ClearCount();

        void HighlightCount();
    }
}
