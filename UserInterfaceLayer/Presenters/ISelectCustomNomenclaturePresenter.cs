﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;

namespace UserInterfaceLayer.Presenters
{
    public interface ISelectCustomNomenclaturePresenter : IPresenter
    {
        //Warehouse CurrentWarehouse { get; set; }

        void Initialize(Warehouse warehouse);
    }
}
