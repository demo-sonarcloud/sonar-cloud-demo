﻿namespace UserInterfaceLayer.Forms
{
    partial class ChequeViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._chequeDataGridView = new System.Windows.Forms.DataGridView();
            this._cancelChequeButton = new System.Windows.Forms.Button();
            this.CustomColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WarehouseColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PriceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CountColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SumColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this._chequeDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // _chequeDataGridView
            // 
            this._chequeDataGridView.AllowUserToAddRows = false;
            this._chequeDataGridView.AllowUserToDeleteRows = false;
            this._chequeDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this._chequeDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._chequeDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CustomColumn,
            this.WarehouseColumn,
            this.PriceColumn,
            this.CountColumn,
            this.SumColumn});
            this._chequeDataGridView.Location = new System.Drawing.Point(12, 30);
            this._chequeDataGridView.Name = "_chequeDataGridView";
            this._chequeDataGridView.Size = new System.Drawing.Size(546, 193);
            this._chequeDataGridView.TabIndex = 0;
            // 
            // _cancelChequeButton
            // 
            this._cancelChequeButton.Enabled = false;
            this._cancelChequeButton.Location = new System.Drawing.Point(12, 229);
            this._cancelChequeButton.Name = "_cancelChequeButton";
            this._cancelChequeButton.Size = new System.Drawing.Size(546, 23);
            this._cancelChequeButton.TabIndex = 1;
            this._cancelChequeButton.Text = "Отменить чек";
            this._cancelChequeButton.UseVisualStyleBackColor = true;
            this._cancelChequeButton.Click += new System.EventHandler(this._cancelChequeButton_Click);
            // 
            // CustomColumn
            // 
            this.CustomColumn.HeaderText = "Товар";
            this.CustomColumn.Name = "CustomColumn";
            this.CustomColumn.ReadOnly = true;
            // 
            // WarehouseColumn
            // 
            this.WarehouseColumn.HeaderText = "Склад";
            this.WarehouseColumn.Name = "WarehouseColumn";
            this.WarehouseColumn.ReadOnly = true;
            // 
            // PriceColumn
            // 
            this.PriceColumn.HeaderText = "Цена";
            this.PriceColumn.Name = "PriceColumn";
            this.PriceColumn.ReadOnly = true;
            // 
            // CountColumn
            // 
            this.CountColumn.HeaderText = "Количество";
            this.CountColumn.Name = "CountColumn";
            this.CountColumn.ReadOnly = true;
            // 
            // SumColumn
            // 
            this.SumColumn.HeaderText = "Сумма";
            this.SumColumn.Name = "SumColumn";
            this.SumColumn.ReadOnly = true;
            // 
            // ChequeViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 259);
            this.Controls.Add(this._cancelChequeButton);
            this.Controls.Add(this._chequeDataGridView);
            this.Name = "ChequeViewForm";
            this.Text = "Просмотр чека";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ChequeViewForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this._chequeDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView _chequeDataGridView;
        private System.Windows.Forms.Button _cancelChequeButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn WarehouseColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn PriceColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CountColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn SumColumn;
    }
}