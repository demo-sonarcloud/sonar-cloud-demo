﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;

namespace DataAccessLayer.Gateway
{
    public class ChequeGateway : IChequeGateway
    {
        private IDatabaseGateway _databaseGateway;

        public ChequeGateway(IDatabaseGateway gateway)
        {
            _databaseGateway = gateway;
        }

        public ChequeId CreateCheque(SellerId id)
        {
            string chequeCmdText = "INSERT INTO CHEQUE (SELL_DATE, ID_SELLER, CANCELLED) " +
                    "VALUES(CURRENT_TIMESTAMP, @id_seller, @cancelled) RETURNING ID_CHEQUE;";

            using (IDbCommand command = _databaseGateway.CreateCommandInActiveTransaction())
            {
                command.CommandText = chequeCmdText;

                IDbDataParameter chequeParamSellerId = command.CreateParameter();
                chequeParamSellerId.ParameterName = "@id_seller";
                chequeParamSellerId.DbType = DbType.Int32;
                chequeParamSellerId.Value = id.Id;
                command.Parameters.Add(chequeParamSellerId);

                IDbDataParameter chequeParamCancelled = command.CreateParameter();
                chequeParamCancelled.ParameterName = "@cancelled";
                chequeParamCancelled.DbType = DbType.Int32;
                chequeParamCancelled.Value = 0;
                command.Parameters.Add(chequeParamCancelled);

                IDbDataParameter chequeParamChequeId = command.CreateParameter();
                chequeParamChequeId.ParameterName = "@id_cheque";
                chequeParamChequeId.DbType = DbType.Int32;
                chequeParamChequeId.Direction = ParameterDirection.Output;
                command.Parameters.Add(chequeParamChequeId);

                try
                {
                    int chequeId = (int)_databaseGateway.ExecuteCommandScalarInActiveTransaction(command);

                    return new ChequeId(chequeId);
                }
                catch (DatabaseGatewayException e)
                {
                    throw new ChequeGatewayException(this.GenerateExceptionText("Create cheque failed.", e), e);
                }
                catch (Exception e)
                {
                    throw new ChequeGatewayException(this.GenerateExceptionText("Unknown exception.", e), e);
                }
            }
        }

        public void CancelCheque(Cheque cheque)
        {
            using (IDbConnection connection = _databaseGateway.CreateConnection())
            {
                try
                {
                    connection.Open();
                    using (IDbCommand cmd = connection.CreateCommand())
                    {
                        string cmdText = "UPDATE CHEQUE SET CANCELLED = 1 WHERE ID_CHEQUE = @id_cheque;";
                        cmd.CommandText = cmdText;

                        IDbDataParameter paramChequeId = cmd.CreateParameter();
                        paramChequeId.ParameterName = "@id_cheque";
                        paramChequeId.DbType = DbType.Int32;
                        paramChequeId.Value = cheque.ID.Id;
                        cmd.Parameters.Add(paramChequeId);

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    throw new ChequeGatewayException(this.GenerateExceptionText("Cancel cheque failed.", e), e);
                }
            }
        }

        public void AddChequeItems(ChequeId id, IEnumerable<ChequeItem> items)
        {
            string cmdText = "INSERT INTO CHEQUE_ITEM (ID_CHEQUE, ID_TOVAR, COUNT_TOVAR, ID_WH) " +
                    "VALUES(@id_cheque, @id_tovar, @count_tovar, @id_warehouse);";

            using (IDbCommand command = _databaseGateway.CreateCommandInActiveTransaction())
            {
                command.CommandText = cmdText;

                IDbDataParameter paramChequeId = command.CreateParameter();
                paramChequeId.ParameterName = "@id_cheque";
                paramChequeId.DbType = DbType.Int32;

                IDbDataParameter paramCustomId = command.CreateParameter();
                paramCustomId.ParameterName = "@id_tovar";
                paramCustomId.DbType = DbType.Int32;

                IDbDataParameter paramCustomCount = command.CreateParameter();
                paramCustomCount.ParameterName = "@count_tovar";
                paramCustomCount.DbType = DbType.Decimal;

                IDbDataParameter paramWarehouseId = command.CreateParameter();
                paramWarehouseId.ParameterName = "@id_warehouse";
                paramWarehouseId.DbType = DbType.Int32;


                foreach (ChequeItem item in items)
                {
                    command.Parameters.Clear();

                    paramChequeId.Value = id.Id;
                    command.Parameters.Add(paramChequeId);

                    paramCustomId.Value = item.CustomID.Id;
                    command.Parameters.Add(paramCustomId);

                    paramCustomCount.Value = item.CustomCount.Count;
                    command.Parameters.Add(paramCustomCount);

                    paramWarehouseId.Value = item.WarehouseID.Id;
                    command.Parameters.Add(paramWarehouseId);

                    try
                    {
                        _databaseGateway.ExecuteCommandNonQueryInActiveTransaction(command);
                    }
                    catch (DatabaseGatewayException e)
                    {
                        throw new ChequeGatewayException(this.GenerateExceptionText("Cheque item add failed.", e), e);
                    }
                    catch (Exception e)
                    {
                        throw new ChequeGatewayException(this.GenerateExceptionText("Unknown exception.", e), e);
                    }
                }
            }
        }

        public IEnumerable<Cheque> GetAllCheques()
        {
            List<Cheque> cheques = new List<Cheque>();

            string cmdText = "SELECT ID_CHEQUE, SELL_DATE, ID_SELLER, CANCELLED FROM CHEQUE;";

            DataSet dataSet;

            try
            {
                dataSet = _databaseGateway.ExecuteCommand(cmdText);
            }
            catch (DatabaseGatewayException e)
            {
                throw new ChequeGatewayException(this.GenerateExceptionText("Get all cheques error.", e), e);
            }
            catch (Exception e)
            {
                throw new ChequeGatewayException(this.GenerateExceptionText("Unknown exception.", e), e);
            }

            DataTable table = dataSet.Tables[0];

            foreach (DataRow row in table.Rows)
            {
                ChequeId chequeID = new ChequeId((int)row["ID_CHEQUE"]);
                DateTime date = (DateTime)row["SELL_DATE"];
                SellerId sellerID = new SellerId((int)row["ID_SELLER"]);
                ChequeCancellationState cancellationState = new ChequeCancellationState((int)row["CANCELLED"] == 0 ? false : true);

                Cheque cheque = new Cheque(chequeID, date, sellerID, cancellationState);

                cheques.Add(cheque);
            }

            return cheques.ToArray();
        }

        public IEnumerable<ChequeItem> GetChequeItems(Cheque cheque)
        {
            List<ChequeItem> items = new List<ChequeItem>();

            string cmdText = "SELECT ID, ID_CHEQUE, ID_TOVAR, COUNT_TOVAR, ID_WH FROM CHEQUE_ITEM WHERE ID_CHEQUE = @id_cheque;";
            
            using (IDbConnection connection = _databaseGateway.CreateConnection())
            {
                try 
                {
                    connection.Open();
                    using (IDbCommand cmd = connection.CreateCommand())
                    {
                        cmd.CommandText = cmdText;
                        IDbDataParameter paramIdCheque = cmd.CreateParameter();
                        paramIdCheque.ParameterName = "@id_cheque";
                        paramIdCheque.DbType = DbType.Int32;
                        paramIdCheque.Value = cheque.ID.Id;
                        cmd.Parameters.Add(paramIdCheque);

                        IDbDataAdapter dataAdapter = _databaseGateway.CreateDataAdapater();
                        dataAdapter.SelectCommand = cmd;

                        DataSet ds = new DataSet();
                        dataAdapter.Fill(ds);

                        DataTable table = ds.Tables[0];

                        foreach (DataRow row in table.Rows)
                        {
                            ChequeItemId chequeItemId = new ChequeItemId((int)row["ID"]);
                            ChequeId chequeId = new ChequeId((int)row["ID_CHEQUE"]);
                            CustomId customId = new CustomId((int)row["ID_TOVAR"]);
                            CustomCount customCount = new CustomCount((decimal)row["COUNT_TOVAR"]);
                            WarehouseId warehouseId = new WarehouseId((int)row["ID_WH"]);

                            ChequeItem item = new ChequeItem(chequeItemId, chequeId, customId, customCount, warehouseId);
                            items.Add(item);
                        }

                        return items;
                    }
                }
                catch (Exception e)
                {
                    throw new ChequeGatewayException(this.GenerateExceptionText("Get cheque items failed.", e), e);
                }
            }
        }

        private string GenerateExceptionText(string text, Exception e)
        {
            return new StringBuilder()
                    .Append(text)
                    .AppendLine()
                    .Append(e.Message)
                    .ToString();
        }
    }
}
