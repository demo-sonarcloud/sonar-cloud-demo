﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;

namespace DataAccessLayer.Gateway
{
    public interface IWarehouseGateway
    {
        IEnumerable<Warehouse> GetAllWarehouses();
        IEnumerable<Warehouse> GetRetailWarehouses();
        IEnumerable<Warehouse> GetWholesaleWarehouses();
    }
}
