﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;

using UserInterfaceLayer.Events;

namespace UserInterfaceLayer.Presenters
{
    public interface ISetCustomCountPresenter : IPresenter
    {
        event EventHandler<CustomCountSetEventArgs> CustomCountSet;

        //Custom Custom { get; set; }

        void Initialize(Custom custom);
    }
}
