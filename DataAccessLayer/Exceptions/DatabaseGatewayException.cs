﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class DatabaseGatewayException : Exception
    {
        public DatabaseGatewayException(string message)
            : base(message)
        {
        }

        public DatabaseGatewayException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
