﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using UserInterfaceLayer.Events;
using UserInterfaceLayer.Views;

namespace UserInterfaceLayer.Forms
{
    public partial class ChequeViewForm : Form, IChequeView
    {
        private readonly ApplicationContext _context;

        public new event Action Closing;
        public event Action ChequeCancelPressed;
        public event EventHandler<ChequeCancellingEventArgs> ChequeCancelling;

        public ChequeViewForm(ApplicationContext context)
        {
            _context = context;
            InitializeComponent();
        }

        public new void Show()
        {
            _context.MainForm = this;
            base.Show();
        }

        public new void Close()
        {
            base.Close();
        }

        public void ShowError(string errorMessage)
        {
            MessageBox.Show(errorMessage);
        }

        public void ShowError(string errorMessage, string errorCaption)
        {
            MessageBox.Show(errorMessage, errorCaption);
        }

        public void ShowCancellingDialog()
        {
            if (MessageBox.Show("Sure?", "Cancel cheque", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                if (ChequeCancelling != null)
                {
                    ChequeCancelling(this, new ChequeCancellingEventArgs(-1));
                }
            }
        }

        public void EnableCancelling(bool enabled)
        {
            _cancelChequeButton.Enabled = enabled;
        }

        public void ClearChequeItems()
        {
            _chequeDataGridView.Rows.Clear();
        }

        public void AddChequeItem(string custom, string warehouse, string price, string count, string sum)
        {
            _chequeDataGridView.Rows.Add(custom, warehouse, price, count, sum);
        }

        private void ChequeViewForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing && Closing != null)
            {
                Closing();
                e.Cancel = true;
            }
        }

        private void _cancelChequeButton_Click(object sender, EventArgs e)
        {
            if (ChequeCancelPressed != null)
            {
                ChequeCancelPressed();
            }
        }
    }
}
