﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UserInterfaceLayer.Views;

using DomainModel.Entities;
using DataAccessLayer.Gateway;

using ApplicationServicesLayer;
using BusinessLogicLayer.Commands;

using UserInterfaceLayer.Events;

namespace UserInterfaceLayer.Presenters
{
    public class SellPresenter : BasePresenter<ISellView>, ISellPresenter
    {
        private readonly List<ChequeItem> _chequeItems = new List<ChequeItem>();
        private readonly List<Custom> _customs = new List<Custom>();

        public Seller CurrentSeller { get; set; }
        public Warehouse CurrentWarehouse { get; set; }


        public SellPresenter(IApplicationController controller, ISellView view) : base(controller, view)
        {
            View.Closing += OnClosing;
            View.Exit += OnExit;
            View.CustomAdding += OnCustomAdding;
            View.ChequeCreating += OnChequeCreating;
            View.ChequeItemCountValidating += OnChequeItemCountValidating;
            View.ChequeItemCountChanged += OnChequeItemCountChanged;
            View.ChequeItemRemoved += OnChequeItemRemoved;
            Controller.GetInstance<IEventAggregator>().CustomSelected += OnCustomSelected;
        }

        public void Initialize(Seller seller, Warehouse warehouse)
        {
            CurrentSeller = seller;
            CurrentWarehouse = warehouse;
            // Clearing
            _chequeItems.Clear();
            _customs.Clear();

            View.ClearChequeItems();

            ISelectCustomNomenclaturePresenter presenter = Controller.GetInstance<ISelectCustomNomenclaturePresenter>();
            presenter.Initialize(warehouse);
        }

        private void OnClosing()
        {
            View.ShowExitDialog();
        }

        private void OnExit()
        {
            ISelectCustomNomenclaturePresenter selectCustomNomenclaturePresenter = Controller.GetInstance<ISelectCustomNomenclaturePresenter>();
            selectCustomNomenclaturePresenter.Hide();

            ISetCustomCountPresenter setCustomCountPresenter = Controller.GetInstance<ISetCustomCountPresenter>();
            setCustomCountPresenter.Hide();

            View.Hide();
            ISelectSellerPresenter presenter = Controller.GetInstance<ISelectSellerPresenter>();
            presenter.Show();
            presenter.Activate();
        }

        private void OnCustomAdding()
        {
            ISelectCustomNomenclaturePresenter presenter = Controller.GetInstance<ISelectCustomNomenclaturePresenter>();
            //presenter.CurrentWarehouse = CurrentWarehouse;
            presenter.Show();
            presenter.Activate();
        }

        private void OnChequeCreating()
        {
            if (_chequeItems.Count > 0)
            {
                ISelectCustomNomenclaturePresenter selectCustomNomenclaturePresenter = Controller.GetInstance<ISelectCustomNomenclaturePresenter>();
                selectCustomNomenclaturePresenter.Hide();

                ISetCustomCountPresenter setCustomCountPresenter = Controller.GetInstance<ISetCustomCountPresenter>();
                setCustomCountPresenter.Hide();
                try
                {
                    Controller.GetInstance<ICommandInvoker>().CreateCheque(CurrentSeller.ID, _chequeItems);
                    View.ShowError("Cheque generated!", "Success");
                    this.ClearChequeItems();
                }
                catch (Exception e)
                {
                    View.ShowError(e.Message, "Failed to generate cheque");
                }
            }
            else
            {
                View.ShowError("Empty cheque", "Error");
            }
        }

        private void ClearChequeItems()
        {
            _chequeItems.Clear();
            _customs.Clear();
            View.ClearChequeItems();
        }

        private void OnCustomSelected(object sender, CustomSelectedEventArgs e)
        {
            Console.WriteLine("Selected custom {0} with count {1}", e.Custom.Nomenclature, e.Count.Count);

            int index = _chequeItems.FindIndex(x => x.CustomID == e.Custom.ID);
            if (index == -1)
            {
                ChequeItem chequeItem = new ChequeItem(null, null, e.Custom.ID, e.Count, CurrentWarehouse.ID);
                _chequeItems.Add(chequeItem);
                _customs.Add(e.Custom);

                View.AddChequeItem(e.Custom, CurrentWarehouse, e.Count);
            }
            else
            {
                decimal count = _chequeItems[index].CustomCount.Count += e.Count.Count;
                decimal sum = _customs[index].Price.Price * _chequeItems[index].CustomCount.Count;
                View.UpdateCountColumn(index, count);
                View.UpdateSumColumn(index, sum);
            }

        }

        private void OnChequeItemCountChanged(object sender, ChequeItemCountChangedEventArgs e)
        {
            decimal count;
            if (decimal.TryParse(e.CustomCount, out count))
            {
                _chequeItems[e.ItemIndex].CustomCount.Count = count;
            }
            // Update sum
            decimal sum = _customs[e.ItemIndex].Price.Price * _chequeItems[e.ItemIndex].CustomCount.Count;
            View.UpdateSumColumn(e.ItemIndex, sum);
        }

        private void OnChequeItemCountValidating(object sender, ChequeItemCountValidatingEventArgs e)
        {
            decimal count;
            if (decimal.TryParse(Convert.ToString(e.FormattedValue), out count))
            {
                if (count > 0)
                {
                    View.ValidateCell(true);
                }
                else
                {
                    View.ShowError("Custom count can't be zero.", "Invalid custom count!");
                }
            }
            else
            {
                View.ValidateCell(false);
                View.ShowError("Please enter correct count", "Parsing error"); 
            }
        }

        private void OnChequeItemRemoved(object sender, ChequeItemRemovedEventArgs e)
        {
            if (e.RowIndex < _chequeItems.Count)
            {
                _chequeItems.RemoveAt(e.RowIndex);
            }
            if (e.RowIndex < _customs.Count)
            {
                _customs.RemoveAt(e.RowIndex);
            }
        }
    }
}
