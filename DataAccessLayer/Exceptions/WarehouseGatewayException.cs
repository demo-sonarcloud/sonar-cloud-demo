﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class WarehouseGatewayException : Exception
    {
        public WarehouseGatewayException(string message)
            : base(message)
        {
        }

        public WarehouseGatewayException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
